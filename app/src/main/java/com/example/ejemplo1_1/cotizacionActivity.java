package com.example.ejemplo1_1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class cotizacionActivity extends AppCompatActivity {

    private TextView lblFolio;
    private TextView lblEnganche;
    private TextView lblPagoMensual;
    private EditText txtDescripcion, txtValorAuto, txtPorcentaje;
    private RadioGroup radioGroup;
    private RadioButton rdb12, rdb18, rdb24, rdb36;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private Cotizacion cot;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);

        this.iniciarComponentes();

        this.btnCalcular.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(txtDescripcion.getText().toString().isEmpty() ||
                        txtValorAuto.getText().toString().isEmpty() ||
                        txtPorcentaje.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Ingrese los datos faltantes", Toast.LENGTH_SHORT).show();
                    txtDescripcion.requestFocus();
                    return;
                }
                int plazos = 0;
                float enganche = 0.0f;
                float pagoMensual = 0.0f;
                if(rdb12.isChecked()) { plazos = 12; }
                if(rdb18.isChecked()) { plazos = 18; }
                if(rdb24.isChecked()) { plazos = 24; }
                if(rdb36.isChecked()) { plazos = 36; }

                cot.setDescripcion(txtDescripcion.getText().toString());
                cot.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                cot.setPorcentajePagoInicial(Float.parseFloat(txtPorcentaje.getText().toString()));
                cot.setPlazos(plazos);

                enganche = cot.calcularPagoInicial();
                pagoMensual = cot.calcularPagoMensual();

                lblEnganche.setText(String.valueOf(enganche));
                lblPagoMensual.setText(String.valueOf(pagoMensual));
            }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lblFolio.setText("Folio: " + String.valueOf(cot.generarId()));
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtPorcentaje.setText("");
                lblPagoMensual.setText("0.00");
                lblEnganche.setText("0.00");
                radioGroup.clearCheck();
                rdb12.setChecked(true);
            }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void iniciarComponentes() {
        TextView lblNombre = (TextView) findViewById(R.id.txtUsuario);
        this.lblFolio = (TextView) findViewById(R.id.txtFolio);
        this.txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        this.txtValorAuto = (EditText) findViewById(R.id.txtValor);
        this.txtPorcentaje = (EditText) findViewById(R.id.txtPorcentaje);
        this.radioGroup = (RadioGroup) findViewById(R.id.rdbGroup);
        this.rdb12 = (RadioButton) findViewById(R.id.rdb12);
        this.rdb18 = (RadioButton) findViewById(R.id.rdb18);
        this.rdb24 = (RadioButton) findViewById(R.id.rdb24);
        this.rdb36 = (RadioButton) findViewById(R.id.rdb36);
        this.lblEnganche = (TextView) findViewById(R.id.txtPagoInicial);
        this.lblPagoMensual = (TextView) findViewById(R.id.txtPagoMensual);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcularCotizacion);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiarCotizacion);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresarCotizacion);

        this.cot = new Cotizacion();

        this.lblFolio.setText("Folio: " + String.valueOf(cot.getFolio()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblNombre.setText("Usuario: " + nombre);
    }

}